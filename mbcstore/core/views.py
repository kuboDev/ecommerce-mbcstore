from django.shortcuts import render, redirect
from .models import Formulario
from core.forms import FormularioForm, CustomUserForm
from django.contrib.auth import login, authenticate

# Create your views here.

def home(request):

    return render(request, 'core/home.html')

def camara(request):
    return render(request, 'core/camara.html')

def consolas(request):
    return render(request, 'core/consolas.html')

def celulares(request):
    return render(request, 'core/celulares.html')

def smart(request):
    return render(request, 'core/smart.html')

def notebook(request):
    return render(request, 'core/notebook.html')

def detalle_notebook(request):
    return render(request, 'core/detalle_notebook.html')

def formulario(request):
    return render(request, 'core/formulario.html')

def formulario_django(request):
    data = {
        'form':FormularioForm()
    }

    if request.method == 'POST':
        formulario = FormularioForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            data['mensaje'] = "Enviado correctamente"
        data['form'] = formulario

    return render(request, 'core/formulario_django.html', data)

def registro_usuario(request):
    data = {
        'form':CustomUserForm()
    }


    if request.method == 'POST':
        formulario = CustomUserForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            #autenticar al usuario y redirigirlo al inicio
            username = formulario.cleaned_data['username']
            password = formulario.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect(to='home')



    return render(request, 'registration/registrar.html', data)