from django import forms
from django.forms import ModelForm
from .models import Formulario
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class FormularioForm(ModelForm):

    #validaciones del formulario
    nombre = forms.CharField(min_length=2,max_length=80)
    correo = forms.EmailField(min_length=3, max_length=80)
    rut = forms.CharField(min_length=7,max_length=15)
    direccion = forms.CharField(min_length=3, max_length=80)
    

    class Meta:
        model = Formulario
        fields = ['nombre','correo','rut','direccion','region']

class CustomUserForm(UserCreationForm):
    
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'username', 'password1', 'password2']