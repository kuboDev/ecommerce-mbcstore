from django.urls import path
from .views import home, camara, consolas, celulares, smart, notebook, detalle_notebook, formulario, formulario_django, registro_usuario

urlpatterns = [
    path('', home, name="home"),
    path('camara/', camara, name="camara"),
    path('consolas/', consolas, name="consolas"),
    path('celulares/', celulares, name="celulares"),
    path('smart/', smart, name="smart"),
    path('notebook/', notebook, name="notebook"),
    path('detalle_notebook/', detalle_notebook, name="detalle_notebook"),
    path('formulario/', formulario, name="formulario"),
    path('formulario_django/', formulario_django, name="formulario_django"),
    path('registro/', registro_usuario, name="registro_usuario"),
]