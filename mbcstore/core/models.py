from django.db import models

# Create your models here.
# python manage.py makemigrations  <-- lee el archivo models
# python manage.py migrate  <-- tomas las migraciones pendientes y volcar  a la base de datos
# python manage.py createsuperuser
# para que aparescan los modelos en el admin hay que agregarrlos a admin.py

class Cliente(models.Model):
    nombre = models.CharField(max_length=80)
    correo = models.EmailField(max_length=80)
    contrasenia = models.CharField(max_length=80, verbose_name="contraseña")
    imagen = models.ImageField(null=True, blank=True)

    def __str__(self):
        return self.nombre

class Region(models.Model):
    nombre = models.CharField(max_length=80, verbose_name="Nombre de la región")

    def __str__(self):
        return self.nombre

class Formulario(models.Model):
    nombre = models.CharField(max_length=80)
    correo = models.EmailField(max_length=80)
    rut = models.CharField(max_length=15)
    direccion = models.CharField(max_length=50, verbose_name="dirección")
    region = models.ForeignKey(Region, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre