from django.contrib import admin
from .models import Cliente, Region, Formulario

# Register your models here.

class ClienteAdmin(admin.ModelAdmin):
    list_display = ['nombre','correo']
    search_fields = ['nombre','correo']
    list_per_page = 20

class RegionAdmin(admin.ModelAdmin):
    list_display = ['nombre']
    search_fields = ['nombre']
    list_per_page = 20

class FormularioAdmin(admin.ModelAdmin):
    list_display = ['nombre','correo']
    search_fields = ['nombre','correo']
    list_per_page = 20
    list_filter = ['region']

admin.site.register(Cliente, ClienteAdmin)
admin.site.register(Region, RegionAdmin)
admin.site.register(Formulario, FormularioAdmin)
